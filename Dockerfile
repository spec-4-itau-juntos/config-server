FROM openjdk:8-jre-alpine
COPY target/configserver-*.jar configserver.jar
CMD [ "java", "-jar", "configserver.jar"]
